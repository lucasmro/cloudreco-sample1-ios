//
//  CloudRecoDelegate.h
//  CloudReco
//
//  Created by Thiago Moretto on 11/25/14.
//  Copyright (c) 2014 Moretto Engenharia de Software. All rights reserved.
//

#import "RecoObject.h"

@protocol CloudRecoDelegate <NSObject>

- (void)onError:(int)errorCode;

- (void)onViewDidLoad:(UIView *) view;

- (void)onViewWillUnload:(UIView *) view;

- (void)objectRecognized:(RecoObject*)recoObject;

@end
