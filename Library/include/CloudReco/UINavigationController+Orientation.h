//
//  UINavigationController+Orientation.h
//  CloudReco
//
//  Created by Thiago Moretto on 12/1/14.
//  Copyright (c) 2014 Moretto Engenharia de Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Orientation)

@end
