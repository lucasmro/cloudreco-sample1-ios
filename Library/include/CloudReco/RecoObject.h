//
//  RecoObject.h
//  CloudReco
//
//  Created by Thiago Moretto on 11/25/14.
//  Copyright (c) 2014 Moretto Engenharia de Software. All rights reserved.
//

@interface RecoObject : NSObject {
  NSString *_id;
  NSString *type;
  NSString *name;
  NSString *objectId;
  NSArray *objectIds;
  NSDictionary *data;
}

@property (nonatomic, strong) NSString *_id;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *objectId;
@property (nonatomic, strong) NSArray *objectIds;
@property (nonatomic, strong) NSDictionary *data;

@end
