//
//  CloudReco.m
//  CloudReco
//
//  Created by Thiago Moretto on 11/5/14.
//  Copyright (c) 2014 Moretto Engenharia de Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CloudRecoApplicationControl.h"
#import "CloudRecoDelegate.h"

#define kCloudRecoAccessKey @"CloudRecoAccessKey"
#define kCloudRecoSecretKey @"CloudRecoSecretKey"
#define kCloudRecoScanLineColor @"CloudRecoScanLineColor"
#define kCloudRecoPointsColor @"CloudRecoPointsColor"

#define kErrorNoMatch 0                   // No matches since the last update
#define kErrorNoRequest 1                 // No recognition request since the last update
#define kErrorNewResultAvaiable 2         // New search results have been found
#define kErrorAuthorizationFailed -1      // Credentials are wrong or outdated
#define kErrorProjectSuspended -2         // The specified project was suspended.
#define kErrorNetworkConnectionFailed -3  // Device has no network connection
#define kErrorServiceNotAvailable -4      // Server not found, down or overloaded.
#define kErrorBadFrameQuality -5          // Low frame quality has been continuously observed
#define kErrorSDKOutDated -6              // SDK Version outdated.
#define kErrorTimestampOutOfRange -7      // Client/Server clocks too far away.
#define kErrorRequestTimedOut -8          // No response to network request after timeout.
#define kErrorParseMetadata -60           // Erro when parsing metadata content.

@interface CloudRecoViewController : UIViewController <CloudRecoApplicationControl> {
    
}

- (id) initWithParams:(NSDictionary *)values
        andDelegateTo:(id<CloudRecoDelegate>)delegate
   andWithOrientation:(UIInterfaceOrientation)orientation;

- (BOOL) isVisualSearchOn;

- (void) toggleVisualSearch;

- (void) pauseRecognition;

- (void) resumeRecognition;

@end
