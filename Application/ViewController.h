//
//  ViewController.h
//  CloudReco-Sample
//
//  Created by Thiago Moretto on 11/5/14.
//  Copyright (c) 2014 Moretto Engenharia de Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CloudRecoDelegate.h"

@interface ViewController : UIViewController <CloudRecoDelegate>

- (IBAction)recoStart;

@end
