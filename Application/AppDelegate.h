//
//  AppDelegate.h
//  CloudReco-Sample
//
//  Created by Thiago Moretto on 11/5/14.
//  Copyright (c) 2014 Moretto Engenharia de Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
