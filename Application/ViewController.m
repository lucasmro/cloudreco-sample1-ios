//
//  ViewController.m
//  CloudReco-Sample
//
//  Created by Thiago Moretto on 11/5/14.
//  Copyright (c) 2014 Moretto Engenharia de Software. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

#import "ViewController.h"
#import "RecoObject.h"
#import "CloudRecoViewController.h"

#define kBottomBarHeightPad 100
#define kBottomBarHeightPhone 50

#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface ViewController ()
{
  CloudRecoViewController *cloudRecoController;
  UIView *sampleView;
}
@end

@implementation ViewController


BOOL CameraAvailable = NO;
BOOL CameraCheckDone = NO;

- (IBAction)recoStart
{
  // Determina o acesso a camera no iOS >= 7
  // Preferencialmente colocar no inicio do aplicativo, ou em qualquer momento antes de chamar o CloudRecoViewController.
  if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType:completionHandler:)]) {
      [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (YES == granted) {
                [self startCloudReco];
            }
            else {
                // Acesso negado a camera, informar o usuario e guia-lo para liberar em:
                // Settings > Privacy > Camera > [Nome do aplicativo]
                UIAlertView *anAlertView = [[[UIAlertView alloc] initWithTitle:@"Sem acesso a câmera"
                                                                       message:@"Você precisa libera acesso da camera para esse aplicativo nas configurações do aparelho."
                                                                      delegate:self
                                                             cancelButtonTitle:@"OK"
                                                             otherButtonTitles:nil] autorelease];
                [anAlertView show];
            }
        });
      }];
  }
  
  else {
    [self startCloudReco];
  }
}

- (void)startCloudReco {
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"557e24d37ad1f01c93950db92671c06cd2d3ecb7", kCloudRecoAccessKey,
                            @"87318f5aaa1b2bb499aba768346d1e997f99b2b5", kCloudRecoSecretKey,
                            UIColorFromRGB(0x00CC00), kCloudRecoScanLineColor, // #00CC00
                            UIColorFromRGB(0xFF3300), kCloudRecoPointsColor, // #FF3300
                            nil];
    
    
    cloudRecoController = [[CloudRecoViewController alloc] initWithParams:params
                                                            andDelegateTo:self
                                                       andWithOrientation:self.interfaceOrientation];
    
    [self.navigationController pushViewController:cloudRecoController animated:NO];
    [cloudRecoController release];
}

#pragma CloudRecoDelegate implementation

- (void)objectRecognized:(RecoObject*)recoObject
{
  dispatch_async( dispatch_get_main_queue(), ^{
    UILabel *label = (UILabel *) [sampleView viewWithTag:42];
    [label setText:recoObject.name];
    [label setTextAlignment:NSTextAlignmentCenter];
      
    [UIView animateWithDuration:1 delay:0.0 options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         sampleView.hidden = NO;
                         sampleView.alpha = 0.5f;
                     }
                     completion:nil];
  });
}

// Customize your view then the CloudReco views was loaded.
- (void)onViewDidLoad:(UIView *)view
{
  [self setupBackButtonAt:view];
  [self setupObjectRecognizedViewAt:view];
}

// Release.
- (void)onViewWillUnload:(UIView *)view
{
}

// Error handling, check CloudRecoViewController.m for the complete error code table.
- (void)onError:(int)errorCode
{
  NSString *title = @"Erro";
  NSString *message = @"Erro na tentativa de reconhecer o produto.";
  
  if(errorCode == kErrorNetworkConnectionFailed) {
    title = @"Rede indisponível";
    message = @"Verique sua conexão com a Internet e tente novamente.";
  }
  
  dispatch_async(dispatch_get_main_queue(), ^{
     if (title && message)
     {
       UIAlertView *anAlertView = [[[UIAlertView alloc] initWithTitle:title
                                                                message:message
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil] autorelease];
       anAlertView.tag = 84;
       [anAlertView show];
     }
  });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
  if(alertView.tag == 42 || alertView.tag == 84)
  {
    [self.navigationController popViewControllerAnimated:YES];
  }
}

#pragma Setups

// Default back button:
- (void)setupBackButtonAt:(UIView *)view
{
  UITapGestureRecognizer *tap =
  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goBack)];
  
  UIButton *myBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
  
  if(IDIOM == IPAD) {
    myBackButton.frame = CGRectMake(10, 30, 200, 50);
  } else {
    myBackButton.frame = CGRectMake(7, 20, 100, 40);
  }

  myBackButton.backgroundColor = [UIColor blackColor];
  myBackButton.alpha = 0.7f;
  
  [myBackButton setTitle:@"Voltar" forState:UIControlStateNormal];
  [myBackButton addGestureRecognizer:tap];
  [view addSubview:myBackButton];
}

// Creates the view that shows up when an object is recognized:
- (void)setupObjectRecognizedViewAt:(UIView *)view
{
  CGRect viewFrame;
  CGRect labelFrame;
  
  CGRect screenBounds = [[UIScreen mainScreen] bounds];
  viewFrame = screenBounds;
    
  if(IDIOM == IPAD)
  {
      viewFrame = CGRectMake(0, viewFrame.size.height - kBottomBarHeightPad, viewFrame.size.width, kBottomBarHeightPad);
      labelFrame = CGRectMake(0, 0, viewFrame.size.width, kBottomBarHeightPad - 20);
  }
  else // iPhone
  {
      viewFrame = CGRectMake(0, viewFrame.size.height - kBottomBarHeightPhone, viewFrame.size.width, kBottomBarHeightPhone);
      labelFrame = CGRectMake(0, 0, viewFrame.size.width, kBottomBarHeightPhone - 20);
  }
  
  sampleView = [[UIView alloc] initWithFrame:viewFrame];
  sampleView.backgroundColor = [UIColor whiteColor];
  sampleView.alpha = 0.1f;
  
  UILabel *objectLabel = [[UILabel alloc] initWithFrame:labelFrame];
  objectLabel.tag = 42;
  objectLabel.textAlignment = NSTextAlignmentCenter;
  objectLabel.textColor = [UIColor blackColor];
  [sampleView addSubview:objectLabel];
  [view addSubview:sampleView];
  [sampleView release];
}

#pragma Selectors

- (void)goBack
{
  [self.navigationController popViewControllerAnimated:YES];
}

#pragma utils

- (BOOL)isRetinaDisplay
{
    return ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && 2.0 == [UIScreen mainScreen].scale);
}

@end
